<?php
/**
 * 
 * @file Drupal install file
 */

define ('SETTINGS_URL', l("Administration -> Store administration \ Configuration \ Payment settings \ Edit \ Payment methods \ Secure Check settings", 'admin/store/settings/payment/edit/methods') );

//aded  her, cause _sc_moduleName() is not accessible from install file
define('MODULE_NAME', "Secure Check Lite");
define('MODULE_ID', 'securecheck_lite');
define('PAYMENT_METHOD_ID', 'secure_check');
define('PAYMENT_METHOD_NAME', "Secure Check");


/**
 *  
 * Implementation of hook_enable().
 * Used message about need to configure module
 */


/**
 * Implementation of hook_requirements().
 * 
 * Here we can set all neccesary info about Required stuff for Secure Check Merchant SOAP API
 * 1) SOAP library 
 * 2) Cron
 * Here is other possible items to require in future 
 * - MerchantID
 * - ServerName
 * - Port of URL
 * - URL to WSDL
 *
 */
function uc_securecheck_lite_requirements($phase) {

	$t = get_t();
	$requirements = array();

	$has_soap = class_exists('SoapClient');
	
	switch ($phase) {
			
		case "install":

			//Using SOAP
			$requirements = _sc_check_soap($has_soap);

			break;

		case "runtime":

			// Using cron
			$cron_last = variable_get('cron_last', NULL);
			$a1 = _sc_check_soap($has_soap);
			$a2 = _sc_check_cron($cron_last);
			$requirements = array_merge($a1, $a2 );
			
	  		break;


	}


	return $requirements;


}
/**
 * 
 * Helper function to check if soap library is found and enabled on this site
 * @param boolean $has_soap
 * @return array
 */
function _sc_check_soap ($has_soap){

	$t = get_t();
	
	// Using SOAP/cron functionality.
	$requirements['uc_securecheck_soap']['title'] =  $t(MODULE_NAME.' and soap');
	$requirements['uc_securecheck_soap']['value'] = $has_soap  ? $t('Enabled') : $t('Not found');

	if ( $has_soap ) {
		$requirements['uc_securecheck_soap']['severity'] = REQUIREMENT_OK;
	} else {
		$requirements['uc_securecheck_soap']['severity'] = REQUIREMENT_ERROR;
		$requirements['uc_securecheck_soap']['description'] = $t("Required to have the PHP <a href='!soap_url'>SOAP</a> library.", array('!soap_url' => 'http://php.net/manual/en/soap.setup.php') );
	}
	 
	return $requirements;
	  
}

/**
 * 
 * Helper function to check if cron enabled on this site
 * @param int $cron_last
 * @return array
 */
function _sc_check_cron($cron_last){
	
	$t = get_t();

	$requirements['uc_securecheck_cron']['title'] = $t(MODULE_NAME.' and cron');


	// Cron warning threshold defaults to two days.
    $threshold_warning = variable_get('cron_threshold_warning', 172800);
    // Cron error threshold defaults to two weeks.
    $threshold_error = variable_get('cron_threshold_error', 1209600);
    
	if ( time() - $cron_last > $threshold_error || time() - $cron_last > $threshold_warning ){
		
		$requirements['uc_securecheck_cron']['severity'] = REQUIREMENT_WARNING;
		$requirements['uc_securecheck_cron']['description'] .= $t('To have order status updated frequently - cron job should be at least daily.');
		$requirements['uc_securecheck_cron']['value'] = $t('Not run recently');
		
	} else if ( !is_numeric($cron_last) ){

		$requirements['uc_securecheck_cron']['severity'] = REQUIREMENT_ERROR;
		$requirements['uc_securecheck_cron']['description'] = $t("To have order status updated frequently - cron must be configured.");
		$requirements['uc_securecheck_cron']['value'] = $t('Never run');

	} else {

		$requirements['uc_securecheck_cron']['severity'] = REQUIREMENT_OK;
		$requirements['uc_securecheck_cron']['value'] = $t('Cron Enabled. Last run !time ago', array('!time' => format_interval(time() - $cron_last)));
	}		
    
	
	
	return $requirements;
	  
}



function uc_securecheck_lite_enable() {
	
	$do_we_have_merchant_id = variable_get('uc_securecheck_lite_merchantID', "");
	
	if (empty($do_we_have_merchant_id) || $do_we_have_merchant_id == null)  {
		drupal_set_message(t('Module settings are here:<br> !url ', array('!url' => SETTINGS_URL)));
	}

	//drupal_flush_all_caches();
}


/**
 * 
 * Implementation of hook_disable().
 * Used message about need to uninstall module, if there is nood for this
 */

function uc_securecheck_lite_disable() {
  
	drupal_set_message( t('Module is disabled. MerchantID is still stored in database. To remove module related  stuff, please go to uninstall.') );

}


/**
 * Implementation of hook_install().
 */
function uc_securecheck_lite_install() {
	
	// Used in integration script to Noca
  	variable_set('uc_securecheck_lite_servername', 'sandbox');
  	
  	// Used in soap request to Noca
  	variable_set('uc_securecheck_lite_apiKey', "" );
  	
  	// Used in that places where system need to provide "Secure Check" name
  	variable_set('uc_securecheck_lite_method_title', PAYMENT_METHOD_NAME );
	  
  	// Used to enable/disable ability of pop-up using
  	variable_set('uc_securecheck_lite_use_popup', '1');
  
  	// Debug mode switch - show(or not) extended server responses
  	variable_set('uc_securecheck_lite_response_on_check', "0");

  	// Used to enable/disable soap/cron functionality for automated order status update
  	variable_set('uc_securecheck_lite_soap_update', "0");
  	
  	// Install table mostly used by soap/cron functionality
  	drupal_install_schema('uc_securecheck_lite');
  	
  	
}


/**
 * Implementation of hook_uninstall().
 */
function uc_securecheck_lite_uninstall() {
	$merchantID = variable_get('uc_securecheck_lite_merchantID', '');	
  	if (!empty($merchantID)){	
  		drupal_set_message(t('Your MerchantID  '.$merchantID.' is removed form site'));
  	}
				
  	variable_del('uc_securecheck_lite_merchantID');
  	variable_del('uc_securecheck_lite_servername');
  	variable_del('uc_securecheck_lite_use_popup');
  	variable_del('uc_securecheck_lite_method_title');
  	variable_del('uc_securecheck_lite_apiKey');
  	// Part of variables whcih can be picked up not properly after re-install
  	variable_del('uc_payment_securecheck_lite_gateway');
  	variable_del('uc_payment_method_securecheck_lite_weight');
  	variable_del('uc_payment_method_securecheck_lite_checkout');
  	variable_del('uc_securecheck_lite_response_on_check');
  	variable_del('uc_securecheck_lite_soap_update');
  	
  	
  	// TODO
  	// DELETE ALL ORDERS FROM MERCHANT SITE when securecheck table is deleted - CLEANUP
  	// use first _sc_getPaymentInfo($order) and the delete order records
  	
  	try{
  		drupal_uninstall_schema('uc_securecheck_lite');
  	} catch (Exception $e){
  		
  	}
}

/**
 * 
 * Future realization database schema to store Merchant/Marketplacce/User info records
 * 
 * PostPoned
 */

function uc_securecheck_lite_schema() {
	
 $schema['uc_payment_securecheck'] = array(

 	'description' => 'Logs Secure Check Payments Information.',
 
    'fields' => array(
      'payment_id' => array(
        'description' => 'The Secure Check Payment Identificator.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      
      'payment_status' => array(
        'description' => 'The Merchant Payment Status (Noca Transmission Status).',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      
      'order_id' => array(
        'description' => 'The order ID. As well which is going to InvoiceID and MerchantRefference and setl_entry.inv_num',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      
      'invoice_id' => array(
        'description' => 'The Secure Check invoiceID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      
      'trn_id' => array(
        'description' => 'The Secure Check transactionID.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      
      'trn_status' => array(
        'description' => 'The Secure Check Transaction Status.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      /*
      'trn_amount' => array(
        'description' => 'The Secure Check Transaction Amount. Need for soap request and further ubercart code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      */
      
      'setl_status' => array(
        'description' => 'The Secure Check Settlement Status. Future use - just after SOAP request and receiving settlement status',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      
      
      'merchant_id' => array(
        'description' => 'The Secure Check MerchantID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      
      'marketplace_id' => array(
        'description' => 'The Secure Check MarketplaceID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      
      
      'merchant_email' => array(
        'description' => 'Secure Check Marketplace/Merchnat account - Receiver.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      
      'user_email' => array(
        'description' => 'Secure Check User account aka merchantIdentity - Payer',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      
      'received' => array(
        'description' => 'The Secure Check Payment last status update timestamp.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    
    'indexes' => array(
      'order_id' => array('order_id'),
      'trn_id' => array('trn_id'),
    ),
    
    'primary key' => array('payment_id'),
    
  );
  
  return $schema;
    
}




/*
function uc_securecheck_lite_update_1() {
  	
	
}

*/
?>