<?php
/**
 * @file
 * Inside of the iFrame in Noca's Secure Check Lite/iFrame API module.
 */

$use_server_name = true;

if ($use_server_name){

	$servername = $_GET['servername'];
	
	if ($servername == "secure2" || $servername == "sandbox" || $servername == "orange"){
		if ($servername == "sandbox"){
			$serverurl = "http://$servername.noca.com";
		} else{
			$serverurl = "https://$servername.noca.com";
		}			
	} else{
		die( "Server Name Configuration for iFrame is not set properly. Should be [secure|sandbox]" );
	}

} else{
	$servername = "sandbox.noca.com";
	$serverurl = "http://".$servername;
}

?>
  
<!-- BEGIN SECURECHECK FORM -->
<!--  <script type="text/javascript" src="/misc/jquery.js"></script>  -->

<?php if ($_GET['popup'] == 1){ ?>	
<script> 
// ORIGINAL code:
//var top = top.opener;

var mytop = top.opener;

// Here we had a lot of javascript trying to deal with Internet Explorer

</script>

<?php } else  {?>
<script type="text/javascript">
<!--
var mytop = top;
//-->
</script>
<?php }?>

<script type="text/javascript">

// Required values
var OCPformReplyTo = mytop.OCPformReplyToTop; // URL user is returned to

var OCPformServerName = mytop.OCPformServerName+".noca.com";


var OCPformTransmitterID = "0"; // leave as 0 if there are no sub-merchants
var OCPformMerchantID = mytop.OCPformMerchantIDTop;

var OCPformMerchantCustomerIdentity = mytop.OCPformMerchantCustomerIdentityTop;
var OCPformInvoiceID = mytop.OCPformInvoiceIDTop;
var OCPformTotalAmount = mytop.OCPformTotalAmountTop;

var OCPformCurrency = "USD";
var OCPformContactInformationSalutation = "";
var OCPformContactInformationFirstName = mytop.OCPformContactInformationFirstNameTop;
var OCPformContactInformationLastName = mytop.OCPformContactInformationLastNameTop;
var OCPformContactInformationEmail = OCPformMerchantCustomerIdentity;

// Optional but recommended values

// The following three fields must all be defined if one of them is defined
var OCPformContactInformationPhoneSMSenabled; // true if mobile phone
var OCPformContactInformationPhoneAreaCode;
var OCPformContactInformationPhone7Digits;

var OCPformExistingUserAtMerchant; // true if the user has previously //transacted with the merchant
var OCPformTaxAmount = 0;

// The following seven fields must all be defined if one of them is defined
// If there is more than one item then include information on the first //item only

// Considering #180 ticket about description (1item) => may-05 =- simple using of these variables dos not help.
var OCPformItemID; // Can be a SKU
var OCPformItemName;
var OCPformItemDescription;
var OCPformItemQuantity;
var OCPformItemPrice;
var OCPformItemDiscount;
var OCPformItemAmount;

// The following 10 fields must all be defined if any one is
var OCPformBillingInformationSalutation; // Mr., Ms., Mrs., Dr., etc
var OCPformBillingInformationFirstName;
var OCPformBillingInformationLastName;
var OCPformBillingInformationAddressLine1;
var OCPformBillingInformationAddressLine2; // set equal to "" if there is //no second address line
var OCPformBillingInformationCity;
var OCPformBillingInformationCounty;
var OCPformBillingInformationState; // Two letter abbreviation
var OCPformBillingInformationCountry; // Two letter abbreviation
var OCPformBillingInformationPostalCode;

// The following 13 fields must all be defined if any one is
var OCPformShippingInformationCompany; // ex: "UPS","DHL","FedEx","USPS"
var OCPformShippingInformationType; // ex: "Ground","2 Day","3 Day"
var OCPformShippingInformationPrice;
var OCPformShippingInformationSalutation;
var OCPformShippingInformationFirstName;
var OCPformShippingInformationLastName;
var OCPformShippingInformationAddressLine1;
var OCPformShippingInformationAddressLine2; // set equal to "" if there is no //second address line
var OCPformShippingInformationCity;
var OCPformShippingInformationCounty;
var OCPformShippingInformationState; // Two letter abbreviation
var OCPformShippingInformationCountry; // Two letter abbreviation
var OCPformShippingInformationPostalCode;

var OCPformDescription = top.OCPformDescriptionTop;

</script>



<form id="OCPform" action="<?=$serverurl?>/merchant/PaymentPresentmentLite#noca_payment_screen" method="post">

<input type="hidden" id="integration" />

<input type="image" id="OCPsubmit" src="<?=$serverurl?>/static/merchant/secure_check.gif"/>
<!-- this is how in vt.php -->
<!-- <input type="submit" value="Submit" id="OCPsubmit"></input>  -->

</form>

<h3>Please wait loading...</h3> <span><img src="<?=$serverurl?>/images/processing.gif" /></span>

<script type="text/javascript" src="<?=$serverurl?>/static/merchant/NocaIntegrationLite.js"></script>

<script type="text/javascript">

document.getElementById('OCPsubmit').click();

</script>
