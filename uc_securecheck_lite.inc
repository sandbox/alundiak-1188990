<?php
// $Id: uc_securecheck_lite.inc,v 1.0 $

/**
 * @file
 * Defines a class used for fetching Noca/SecureCheck transaction/settlement statuses via SOAP.
 *
 * Provided by Softjourn Inc, Commercially supported by Noca Inc.
 */

class SecureCheckSoapClient extends SoapClient {
	
  function __construct($wsdl, $options = NULL) {
  	  	
  	$url = parse_url($wsdl);
        if ($url['port']) {
            $this->_port = $url['port'];
        }
  	
        
    parent::__construct($wsdl, $options);
  }

  // This section inserts the UsernameToken information in the outgoing request.
  function __doRequest($request, $location, $action, $version) {

  	
  	 $parts = parse_url($location);
        if ($this->_port) {
            $parts['port'] = $this->_port;
        }
        
        
	$merchantID = MERCHANTID;
	$api_key = API_KEY;

	// TODO with autorization
    $soapHeader = "<SOAP-ENV:Header xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>
    <wsse:Security >
     <wsse:UsernameToken>
      <wsse:Username> $merchantID </wsse:Username>
      <wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>$api_key</wsse:Password>
      </wsse:UsernameToken>
      </wsse:Security>
      </SOAP-ENV:Header>";

    
    
    $soapHeader = "<SOAP-ENV:Header xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>
                 </SOAP-ENV:Header>";
    
    return parent::__doRequest($request, $location, $action, $version);
  }
  
  
      
 /*
    public function buildLocation($parts = array()) {
        $location = '';
 
        if (isset($parts['scheme'])) {
            $location .= $parts['scheme'].'://';
        }
        if (isset($parts['user']) || isset($parts['pass'])) {
            $location .= $parts['user'].':'.$parts['pass'].'@';
        }
        $location .= $parts['host'];
        if (isset($parts['port'])) {
            $location .= ':'.$parts['port'];
        }
        $location .= $parts['path'];
        if (isset($parts['query'])) {
            $location .= '?'.$parts['query'];
        }
 
        return $location;
    }
  */
    
    
}
