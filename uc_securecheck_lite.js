/*
 * Secure Check Javascript Helper File
 * 
 * Since Apr-2011 (c) Noca Inc. and Softjourn Inc. collaboration work
 * 
 * Author: Andrii Lundiak (@landike)
 * 
 */


function SimpleModalPopup(pageURL, title,w,h, window2) {

	var _left = (screen.width/2)-(w/2);
	var _top = (screen.height/2)-(h/2);
		
	h_window = h + 1;
	w_window = w + 10;
	var sett = 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width='+w_window+', height='+h_window+', top='+_top+', left='+_left ;
	var s = "' "+sett+" '";
	var t = "'"+title+"'";

	var myWin = window2.open ("", 'securecheckwindow', s );
	
	myWin.document.write('<HTML id="w_popup"><TITLE>'+title+'</TITLE><HEAD></HEAD<BODY></BODY></HTML>');
	setSF = function setSubmitFocus(){
		
		//alert(self.frames[0].frames[0].document.getElementById('noca_submit').id );
		/*
		var arrFrames = parent.document.getElementsByTagName("IFRAME");
		for (var i = 0; i < arrFrames.length; i++) {
			//alert(arrFrames[i].location);
		  if (arrFrames[i].contentWindow === window) alert("yay!");
		}
		*/
		// 															   FF || IE
		//var frDOM = document.getElementById('in_w_popup').contentDocument || document.frames['in_w_popup'].document;
		
		//alert(document.getElementsByTagName( "iframe" )[0].contentWindow.document.getElementsByTagName( "iframe" )[0])
		
		// alert(window.document.getElementsByTagName( "iframe" )[0].contentWindow.document.getElementsByTagName( "form" )[0].id)
		// Gives OCPform
		
		
		
		//alert(window.frames[0].location); // WORKS inside
		//alert(window.frames[0].frames[0].location); // NOT WORKED
		/*
		alert(window.frames[0].frames[0].document);
		console.log(window.frames[0].frames[0].document);
		console.log(self.frames[0].frames[0].document);
		alert(window.frames[0].frames[0].document.getElementById('noca_submit').id );
		*/
		
		
	};
	
	//myWin.document.write("<script>"+setSF+"</script>");
	
	
	
	h_frame = h - 20;
	
	
	myWin.document.write('<iframe id="in_w_popup" name="in_w_popup" src="'+ pageURL+'" width="'+w+'" height="'+h_frame+'" scrolling="no" frameborder="0"></iframe>');
	
	//myWin.document.write("<script>"+setSF+"  setSubmitFocus();</script>");
	
	
	//alert(myWin.location);
	//alert(myWin.document.getElementById('in_w_popup').id);
	/*alert(1);
	alert(myWin.document.getElementById('in_w_popup').contentWindow.document);
	alert(myWin.document.getElementById('in_w_popup').ownerDocument);
	alert(2);
	alert(myWin.document.getElementById('in_w_popup').contentWindow.contentDocument);
	*/
	
	//myWin.frames.document.getElementById('in_w_popup').contentWindow ...._left want to set focus() on submit button for IE
}

function PaymentUnBlock(error_message_3){
	
	if (Drupal.jsEnabled) { 
		$(document).ready( 
				function() { 
					
						//Settings to be used when other payment method will be used
						Drupal.settings.SecureCheckIsNotReady = true;
						
						if ($( "input[name='panes[payment][payment_method]']:checked" ).val() == Drupal.settings.SecureCheckMethodId ){
							document.getElementById('edit-continue').disabled = true;
							alert (error_message_3);
						}
						function RadioClickHandler()
						  {
							
						      if ($( this ).val() == Drupal.settings.SecureCheckMethodId && Drupal.settings.SecureCheckIsNotReady == true){
						    	  
						    	  Drupal.settings.SecureCheckIsNotReady = false;
						    	  
						    	  document.getElementById('edit-continue').disabled = true;
						    	  alert (error_message_3);
						    	  
						      } else {
						    	  document.getElementById('edit-continue').disabled = false;
						      }
						  }
						
						$( "input[name='panes[payment][payment_method]']" ).bind( "click", RadioClickHandler );
					
					
				});
		
		
	};
	
		
		
}

function isNumeric(merchantID){
    //return typeof(input)=='number';
	
	var ret = true;
	
	if ( isNaN(merchantID) ){
	
		if (Drupal.jsEnabled) { 
			$(document).ready( 
					function() { 
							PaymentUnBlock("Provided MerchantID '"+ merchantID +"' is not a number");
					});
			
		};
		
		ret = false;
	}
	
	return ret;
	
}



function areYouSure(this2){
	
	//id="edit-uc-securecheck-lite-servername";
	
	if (this2.value == 'secure2'){
		
	
		if(confirm('Are you sure to change server to live using?')){
			alert('Now (after submit changes), Secure Check Lite module will use Live Noca server to make transactions!');
			return true;
		} else {
			return false;
		}
			
	}
	
}





/*
function BackToCheckOut(){
	
	if (Drupal.jsEnabled) { 
		$(document).ready( 
				function() { 
					
						
						function BackButtonClickHandler()
						  {
							
							top.window.location = "cart/checkout";
							
						  }
						
						$( "#go_back_to_checkout" ).bind( "click", BackButtonClickHandler );
					
					
				});
		
		
	};
	
		
		
}

opener Property
When opening a window using window.open, use this property from the destination window 
to return details of the source window. This has many uses, for example, window.opener.close() 
will close the source window.

Syntax: window.opener



parent Property
This property is a reference to the window or frame that contains the calling child frame.

Syntax: window.parent


self Property
This property is a reference (or synonym) for the current active window or frame. 
Syntax: self.property or method


top Property
This property is a reference (or synonym) for the topmost browser window.

Syntax: top.property or method 

close Method
This method is used to close a specified window. If no window reference is supplied, the close() method will close the current active window. Note that this method will only close windows created using the open() method; if you attempt to close a window not created using open(), the user will be prompted to confirm this action with a dialog box before closing. The single exception to this is if the current active window has only one document in its session history. In this case the closing of the window will not require confirmation.

Syntax: window.close( )



*/